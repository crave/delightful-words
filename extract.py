#!/usr/bin/env python3

from bs4 import BeautifulSoup
import json

with open("data.html", "r") as f:
    data = BeautifulSoup(f.read())

output = []
words = data.find_all('tr')
for word_data in words:
    word, description = word_data.find_all('td')
    word = word.text
    description = description.text
    try:
        link = word_data.td.a['href']
    except TypeError:
        link = None
    output.append({
        'word': word,
        'link': link,
        'description': description,
    })
with open('data.json', 'w') as f:
    json.dump(output, f)
