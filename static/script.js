function random() {
    // Generate an extremely bad pseudo-random number that's seeded by the current day.
    let date = new Date();
    let a = date.getYear();
    let b = date.getMonth();
    let c = date.getDate();
    let m = 8161;
    return ((((3671 + 1619*a) % m + 719*b*b) % m + 311*c*c*c) % m) / m;
}
window.onload = function() {
    let word = document.getElementsByClassName('word')[0];
    let description = document.getElementsByClassName('description')[0];
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let words = JSON.parse(this.responseText);
            let choice = words[Math.floor(random() * words.length)];
            let wordLink = document.createElement('a');
            wordLink.appendChild(document.createTextNode(choice.word));
            wordLink.setAttribute('href', choice.link);
            word.appendChild(wordLink);
            description.appendChild(document.createTextNode(choice.description));
        }
    };
    xmlhttp.open("GET", "/data.json", true);
    xmlhttp.send();
};
